using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
	public SpriteRenderer sprite;
	public int EnemyHealth= 2; // 3 hit points
	public float MovementSpeed=10;
	public int Damage=4;


	public GameObject duplicatePrefab;
	public GameObject DeathParticle;

	public Transform[] spawnPoints = new Transform[3];

	void Start ()
	{
		//Add a tiny bit of variation to the move speed.
		MovementSpeed *= Random.Range(0.9f, 1.1f);
	}
    private void Awake()
    {
		//
	}

    void Update ()
	{
		//Move to planet.
		transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, MovementSpeed * Time.deltaTime);

		LookAtPlanet();
	}

	//Makes it so that the ship is always looking at the planet.
	void LookAtPlanet ()
	{
		Vector3 dir = transform.position.normalized;
		float ang = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
		transform.eulerAngles = new Vector3(0, 0, ang);
	}

    //Called when a bullet hits the ship.

    private void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.CompareTag("Bullet")) // check if colliding with bullet
		{
			if (EnemyHealth == 0) // check if no HP left
			{
				Destroy(this.gameObject);
				Debug.Log("test");
			}
			else
			{
				Debug.Log("enemy health" + EnemyHealth);
				EnemyHealth -= 1; // lower HP
			}
			
			//Instantiate(collisionParticle, col.gameObject.transform.position, Quaternion.identity); // spawn particle
		}
	}
	
	
	/*Called by the blue ship. Duplicates the ship.
	void Duplicate ()
	{
		GameObject e1 = Instantiate(duplicatePrefab, transform.position + (transform.up * -2), Quaternion.identity);
		GameObject e2 = Instantiate(duplicatePrefab, transform.position + (transform.right * 2), Quaternion.identity);
		GameObject e3 = Instantiate(duplicatePrefab, transform.position + (transform.right * -2), Quaternion.identity);
	}*/
}
