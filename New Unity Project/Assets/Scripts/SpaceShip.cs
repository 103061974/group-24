﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour
{
    public float moveSpeed;
    private float attackTimer;
    public float bulletSpeed;
    public float bulletSpread = 1.0f;
    public Transform rocketSprite;
    public bool canHoldFire; //Can the player hold down space to fire?

    //Prefabs
    public GameObject RocketBulletPrefab;


    public static SpaceShip r;

    void Awake() { r = this; }

    void Update()
    {
        RotateRocket();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<AudioSource>().Play();
            attackTimer = 0.0f;
            ShootBullet();
        }

        attackTimer += Time.deltaTime;
    }

    //Rotates the rocket around the planet.
    void RotateRocket()
    {
        transform.eulerAngles += new Vector3(0, 0, (-moveSpeed * Input.GetAxis("Horizontal")) * Time.deltaTime);
        rocketSprite.localEulerAngles = new Vector3(0, 0, Input.GetAxis("Horizontal") * -30);
    }

    //shoots a bullet forward from the player.
    void ShootBullet()
    {
        RocketBulletPrefab = Instantiate(RocketBulletPrefab, rocketSprite.transform.position, transform.rotation);

        Vector2 dir = rocketSprite.transform.position.normalized * (bulletSpeed * Random.Range(1.0f, 1.1f));
        Vector3 offset = RocketBulletPrefab.transform.right * Random.Range(-bulletSpread, bulletSpread);
        dir.x += offset.x;
        dir.y += offset.y;

        RocketBulletPrefab.GetComponent<Rigidbody2D>().velocity = dir;

        if (canHoldFire)
        {
            for (int x = -1; x < 2; x++)
            {
                if (x != 0)
                {
                    RocketBulletPrefab = Instantiate(RocketBulletPrefab, rocketSprite.transform.position, rocketSprite.rotation);

                    dir = rocketSprite.transform.position.normalized * (bulletSpeed * Random.Range(1.0f, 1.1f));
                    offset = RocketBulletPrefab.transform.right * (x * 5 + Random.Range(-bulletSpread, bulletSpread));
                    dir.x += offset.x;
                    dir.y += offset.y;

                    RocketBulletPrefab.GetComponent<Rigidbody2D>().velocity = dir;
                }
            }
        }
    }

}
