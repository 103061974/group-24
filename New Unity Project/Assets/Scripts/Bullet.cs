﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Bullet : MonoBehaviour 
{
	public int damage;
	public float shootForce = 30f;
	public GameObject shootParticle;
	private ParticleSystem shootSystem;
}
