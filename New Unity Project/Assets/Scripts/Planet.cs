using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour 
{
	public int health;

	//Components
	public SpriteRenderer SpriteRenderer;

	public static Planet p;

	//Called when a ship hits the planet.
	public void TakeDamage (int dmg)
	{
		//If the health is less than or equal to 0, then end the game.
		if(health - dmg <= 0)
		{

			Destroy(p);
		}
		else
			health -= dmg;
	}
}
