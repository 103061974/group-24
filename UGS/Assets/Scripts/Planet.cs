using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Planet : MonoBehaviour
{
    public int planetHealth = 100;

    private float transitionTime = 1;
    private bool isTransitioning = false;

    public GameObject controller;
    private GameplayController gameController;

    //Components
    public SpriteRenderer SpriteRenderer;

    public static Planet p;
    public GameObject planet;

    public ParticleSystem explosion;
    public AudioClip explosionClip;
    private AudioSource gameMusic;

    private void Awake()
    {
        gameController = controller.GetComponent<GameplayController>();
        gameMusic = GameObject.Find("Music").GetComponent<AudioSource>();

    }

    private void Update()
    {
        if (isTransitioning)
        {
            if (transitionTime > 0)
            {
                transitionTime -= Time.deltaTime;
            }
            else
            {
                SceneManager.LoadScene("Game Over");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Enemy")) // check if colliding with enemy
        {
            if (planetHealth <= 0) // check if no HP left
            {
                //change texture
                planetHealth = 0;
                gameController.UpdatePlanetHealth(planetHealth);
                // above two lines prevent -1% or -2% from appearing on the planet
                KillPlanet();
            }
            else
            {
                planetHealth -= Random.Range(1, 4); // lower HP
                gameController.UpdatePlanetHealth(planetHealth);
            }
        }
        if (col.gameObject.CompareTag("UFO")) // check if colliding with enemy
        {
            if (planetHealth <= 0) // check if no HP left
            {
                planetHealth = 0;
                gameController.UpdatePlanetHealth(planetHealth);
                // above two lines prevent -1% or -2% from appearing on the planet
                KillPlanet();
            }
            else
            {
                planetHealth -= Random.Range(3, 6); // lower HP
                gameController.UpdatePlanetHealth(planetHealth);
            }
        }
    }

    public void KillPlanet()
    {
        if (gameObject != null)
        {
            //do something
            gameController.UpdatePlanetHealth(planetHealth);
            GetComponent<SpriteRenderer>().enabled = false;
            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Destroy(enemy);
            }
            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("UFO"))
            {
                Destroy(enemy);
            }
            GameObject myShip = GameObject.Find("SpaceShip");
            Destroy(myShip);
            gameController.planetHealthText.gameObject.SetActive(false);

            gameMusic.Stop();

            AudioSource.PlayClipAtPoint(explosionClip, Camera.main.transform.position, 1f);
            Instantiate(explosion, gameObject.transform.position, Quaternion.identity);


            isTransitioning = true;
        }
    }
}
