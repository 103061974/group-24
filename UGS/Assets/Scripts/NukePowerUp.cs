using System.Collections;
using UnityEngine;

public class NukePowerUp : MonoBehaviour
{
    private SpriteRenderer cube1;
    private CircleCollider2D cube2;

    public GameplayController gameController;

    public GameObject HitParticle;
    public AudioClip powerupHit;


    public GameObject popupText;
    private GameObject _instance;


    private void Start()
    {
        cube1 = gameObject.GetComponent<SpriteRenderer>();
        cube2 = gameObject.GetComponent<CircleCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            AudioSource.PlayClipAtPoint(powerupHit, Camera.main.transform.position, 1f); //sfx
            Instantiate(HitParticle, gameObject.transform.position, Quaternion.identity); //particle
            _instance = Instantiate(popupText, transform.position, Quaternion.identity); //popup text
            PowerUp();
            Destroy(_instance, .8f); //Destroy after 1 second.
            Destroy(gameObject); //kill powerup object
        }
    }

    private void PowerUp()
    {
        cube1.enabled = false;
        cube2.enabled = false;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies)
        {
            Destroy(enemy);
        }
        GameObject[] ufos = GameObject.FindGameObjectsWithTag("UFO");
        foreach (GameObject ufo in ufos)
        {
            Destroy(ufo);
        }
    }

}

