using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUFO : MonoBehaviour
{
    public SpriteRenderer sprite;
    public int EnemyHealth = 5; // 5 hit points
    public float MovementSpeed = 2.3f;

    public GameObject DeathParticle;
    public AudioClip EnemyDie;

    private Bullet bullet;
    public GameplayController gameController;

    private void Start()
    {
        //Add a tiny bit of variation to the move speed.
        MovementSpeed *= Random.Range(0.9f, 1.1f);
    }

    private void Update()
    {
        //Move to planet.
        transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, MovementSpeed * Time.deltaTime);
        LookAtPlanet();
    }

    //Makes it so that the ship is always looking at the planet.
    private void LookAtPlanet()
    {
        Vector3 dir = transform.position.normalized;
        float ang = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
        transform.eulerAngles = new Vector3(0, 0, ang);
    }

    public void KillEnemy()
    {
        if (gameObject != null)
        {
            AudioSource.PlayClipAtPoint(EnemyDie, Camera.main.transform.position, 1f);
            Instantiate(DeathParticle, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    //Called when a bullet hits the ship.
    private void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.CompareTag("Bullet")) // check if colliding with bullet
        {
            GameObject bulletObject = GameObject.Find("RocketBullet(Clone)");
            bullet = bulletObject.GetComponent<Bullet>();
            TakeDamage(bullet.damage); //adjust this to current damage value
        }

        if (col.gameObject.CompareTag("Planet")) // check if colliding with bullet
        {

            KillEnemy(); //die lol
        }
    }

    public void TakeDamage(int dmg)
    {
        EnemyHealth -= dmg; // lower HP
        if (EnemyHealth < 1)
        {
            gameController.UpdateScore(25);
            KillEnemy();
        }
    }
}
