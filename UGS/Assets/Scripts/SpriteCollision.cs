using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteCollision : MonoBehaviour

{
    public GameObject MainShipObject;
    private SpaceShip myShip;

    private void Awake()
    {
        myShip = MainShipObject.GetComponent<SpaceShip>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Enemy")) // check if colliding with bullet
        {
            myShip.TakeDamage(1);
            Enemy e = col.GetComponent<Enemy>();
            e.KillEnemy();
        }

        if (col.gameObject.CompareTag("UFO")) // check if colliding with bullet
        {
            myShip.TakeDamage(2);
            EnemyUFO e = col.GetComponent<EnemyUFO>();
            e.KillEnemy();
        }
    }

}
