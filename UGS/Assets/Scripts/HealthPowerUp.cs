using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPowerUp : MonoBehaviour
{
    private SpaceShip myShip;
    public GameplayController gameController;

    public GameObject HitParticle;
    public AudioClip powerupHit;

    public GameObject popupText;
    private GameObject _instance;

    private void Start()
    {
        GameObject ship = GameObject.Find("SpaceShip");
        myShip = ship.GetComponent<SpaceShip>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {

            AudioSource.PlayClipAtPoint(powerupHit, Camera.main.transform.position, 1f); //sfx
            Instantiate(HitParticle, gameObject.transform.position, Quaternion.identity); //particle
            _instance = Instantiate(popupText, transform.position, Quaternion.identity); //popup text


            myShip.shipHealth += 5;
            gameController.UpdateShipHealth(myShip.shipHealth);
            Destroy(_instance, .8f);
            Destroy(gameObject);

        }
    }

}
