using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour
{

    // Planet health variables
    private int planetHealth;
    public Text planetHealthText;

    // Game timer variable
    public Text timerText;

    // Spaceship health variables
    public Text shipHealthText;

    // Scopre variables
    private int scoreValue;
    public Text scoreText;

    public Text PowerUpText;

    public float PowerUpTime;

    private void Start()
    {
        PlayerPrefs.SetInt("Score", 0);
        Application.targetFrameRate = 60; //lock game at 60 FPS
    }

    private void Update()
    {
        timerText.text = "Time: " + Mathf.Round(Time.timeSinceLevelLoad).ToString();
        float time = Mathf.Round(Time.timeSinceLevelLoad);
        PlayerPrefs.SetFloat("Time", time);
        ProcessPowerUp();
    }

    public void StartPowerUpForSeconds(float seconds)
    {
        PowerUpTime = seconds;
    }

    public void UpdatePlanetHealth(int health)
    {
        planetHealth = health;
        if (planetHealth < 11)
        {
            planetHealthText.color = new Color32(191, 0, 0, 255);
        }
        else if (planetHealth < 26)
        {
            planetHealthText.color = new Color32(255, 143, 23, 255);
        }
        else if (planetHealth < 51)
        {
            planetHealthText.color = new Color32(255, 213, 25, 255);
        }
        else if (planetHealth < 76)
        {
            planetHealthText.color = new Color32(124, 255, 31, 255);
        }
        planetHealthText.text = planetHealth.ToString() + "%";
    }

    public void UpdateShipHealth(int health)
    {
        shipHealthText.text = "Ship Health: " + health.ToString();
    }

    public void UpdateScore(int score)
    {
        scoreValue += score;
        scoreText.text = "Score: " + scoreValue.ToString();
        PlayerPrefs.SetInt("Score", scoreValue);
    }

    public void ProcessPowerUp()
    {
        PowerUpText.gameObject.SetActive(true);

        PowerUpText.text = "Power Up: " + Mathf.Round(PowerUpTime).ToString();

        if (PowerUpTime > 0)
        {
            PowerUpTime -= Time.deltaTime;
            PowerUpText.text = "Power Up: " + Mathf.Round(PowerUpTime).ToString();
        }
        else
        {
            PowerUpText.gameObject.SetActive(false);
            PowerUpTime = 0;
        }

    }
}
