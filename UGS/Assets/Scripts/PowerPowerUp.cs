using System.Collections;
using UnityEngine;

public class PowerPowerUp : MonoBehaviour
{
    public float timer;
    private SpriteRenderer cube1;
    private CircleCollider2D cube2;

    public GameplayController gameController;
    private Bullet bullet; //bullet object

    public GameObject HitParticle;
    public AudioClip powerupHit;

    public GameObject popupText;
    private GameObject _instance;

    private bool isPowerupActive = false;

    private void Start()
    {
        cube1 = gameObject.GetComponent<SpriteRenderer>();
        cube2 = gameObject.GetComponent<CircleCollider2D>();
        timer = 10f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            AudioSource.PlayClipAtPoint(powerupHit, Camera.main.transform.position, 1f); //sfx
            Instantiate(HitParticle, gameObject.transform.position, Quaternion.identity); //particle
            _instance = Instantiate(popupText, transform.position, Quaternion.identity); //popup text
            Destroy(_instance, .8f);
            PowerUp();
        }
    }

    private void Update()
    {
        if (isPowerupActive) //check if powerup is active
        {
            if (GameObject.Find("RocketBullet(Clone)") != null) //check if a bullet is present in the world
            {
                GameObject bulletObject = GameObject.Find("RocketBullet(Clone)"); //find bullet
                bullet = bulletObject.GetComponent<Bullet>(); //get script from bullet
                bullet.damage = 99; //set damage to 99
            }

            if (gameController.PowerUpTime == 0) //check if powerup has finished (10 seconds over)
            {
                isPowerupActive = false; //set powerup to inactive, stopping this whole loop
                Destroy(gameObject); //kill powerup object
            }
        }
    }

    private void PowerUp()
    {
        cube1.enabled = false;
        cube2.enabled = false;
        gameController.StartPowerUpForSeconds(timer);
        isPowerupActive = true; //set powerup to active
    }

}

