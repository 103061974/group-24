using System.Collections;
using UnityEngine;

public class SpeedPowerUp : MonoBehaviour
{
    private readonly float increase = 80;
    private SpaceShip myShip;
    public float timer;
    private SpriteRenderer cube1;
    private CircleCollider2D cube2;

    public GameplayController gameController;

    public GameObject HitParticle;
    public AudioClip powerupHit;

    public GameObject popupText;
    private GameObject _instance;

    private bool isPowerupActive = false;

    private void Start()
    {
        GameObject ship = GameObject.Find("SpaceShip");
        myShip = ship.GetComponent<SpaceShip>();

        cube1 = gameObject.GetComponent<SpriteRenderer>();
        cube2 = gameObject.GetComponent<CircleCollider2D>();
        timer = 10f;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            AudioSource.PlayClipAtPoint(powerupHit, Camera.main.transform.position, 1f); //sfx
            Instantiate(HitParticle, gameObject.transform.position, Quaternion.identity); //particle
            _instance = Instantiate(popupText, transform.position, Quaternion.identity); //popup text          

            Destroy(_instance, 1f);
            SpeedUp();
        }
    }

    private void Update()
    {
        if (isPowerupActive) //check if powerup is active
        {
            if (myShip.moveSpeed < 210)
            {
                myShip.moveSpeed += increase;
            }

            if (gameController.PowerUpTime == 0) //check if powerup has finished (10 seconds over)
            {
                isPowerupActive = false; //set powerup to inactive, stopping this whole loop
                myShip.moveSpeed -= increase;
                Destroy(gameObject); //kill powerup object
            }
        }
    }

    private void SpeedUp()
    {
        cube1.enabled = false;
        cube2.enabled = false;

        gameController.StartPowerUpForSeconds(timer);
        isPowerupActive = true;

    }

}
