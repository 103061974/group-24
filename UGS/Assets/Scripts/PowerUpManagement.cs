using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManagement : MonoBehaviour
{
    private GameObject PowerUp;
    public GameObject SpeedCube;
    public GameObject HealthCube;
    public GameObject PowerCube;
    public GameObject NukeCube;

    public GameObject Bullet;
    public GameObject controller;
    private GameplayController gameController;

    private void Start()
    {
        gameController = controller.GetComponent<GameplayController>();
    }
    private void Update()
    {
        if (Random.value < 0.003)
        {
            if (GameObject.FindGameObjectsWithTag("Powerup").Length < 2 && gameController.PowerUpTime == 0)
            {
                SpawnPowerUp();
            }
        }
    }

    private void SpawnPowerUp()
    {
        int randomNumber = Mathf.RoundToInt(Random.Range(0, 2));
        int randomNumberPowerUp = Mathf.RoundToInt(Random.Range(0, 4));


        Vector2 spawnPoint;
        switch (randomNumber)
        {
            case 1:
                spawnPoint = LeftSide();
                break;
            default:
                spawnPoint = RightSide();
                break;
        }

        if ((GameObject.Find("Speed(Clone)") && (randomNumberPowerUp == 1 || randomNumberPowerUp == 0)) || (GameObject.Find("Power(Clone)") && (randomNumberPowerUp == 1 || randomNumberPowerUp == 0)))
        {
            return;
        }

        switch (randomNumberPowerUp)
        {
            case 1:
                PowerUp = SpeedCube;
                break;
            case 2:
                PowerUp = HealthCube;
                break;
            case 3:
                PowerUp = NukeCube;
                break;
            default:
                PowerUp = PowerCube;
                break;
        }

        GameObject p = Instantiate(PowerUp, spawnPoint, Quaternion.identity);
        switch (randomNumberPowerUp)
        {
            case 1:
                p.GetComponent<SpeedPowerUp>().gameController = gameController;
                break;
            case 2:
                p.GetComponent<HealthPowerUp>().gameController = gameController;
                break;
            case 3:
                p.GetComponent<NukePowerUp>().gameController = gameController;
                break;
            default:
                p.GetComponent<PowerPowerUp>().gameController = gameController;
                break;
        }

    }

    private Vector2 LeftSide()
    {
        float x, y;
        x = Random.Range(-18, -9);
        y = Random.Range(-10, 11);
        return new Vector2(x, y);
    }

    private Vector2 RightSide()
    {
        float x, y;
        x = Random.Range(10, 19);
        y = Random.Range(-10, 11);
        return new Vector2(x, y);
    }
}
