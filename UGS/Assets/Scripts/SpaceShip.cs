using UnityEngine;

public class SpaceShip : MonoBehaviour
{
    public int shipHealth = 10;
    public GameObject controller;
    private GameplayController gameController;

    private Planet myPlanet;

    public float moveSpeed = 200;
    public Transform rocketSprite;

    public Sprite rocket, rocketFlash;
    private float attackTimer;
    public float bulletSpeed = 10f;
    public float buttonCoolDown = 0.15f;


    //Prefabs
    public GameObject RocketBulletPrefab;
    private GameObject RocketBullet;

    public float AttackTimer { get => attackTimer; set => attackTimer = value; }

    private void Start()
    {
        shipHealth = 10;
        gameController = controller.GetComponent<GameplayController>();
    }

    private void Update()
    {
        MoveRocket();
        buttonCoolDown -= Time.deltaTime;

        if (Input.GetKey(KeyCode.Space) || Input.GetButton("Xbox"))
        {
            if (buttonCoolDown < 0)
            {
                RocketBullet = Instantiate(RocketBulletPrefab, rocketSprite.transform.position, transform.rotation);
                GetComponent<AudioSource>().Play();
                AttackTimer = 0.0f;
                ShootBullet(RocketBullet);
                buttonCoolDown = 0.15f;
            }
        }
        AttackTimer += Time.deltaTime;

        if (shipHealth < 0)
        {
            shipHealth = 0;
            gameController.UpdateShipHealth(shipHealth);
            GameObject planet = GameObject.Find("Planet");
            Planet myPlanet = (Planet) planet.GetComponent(typeof(Planet));
            myPlanet.KillPlanet();
        }
    }

    //Rotates the rocket around the planet.
    private void MoveRocket()
    {
        transform.eulerAngles += new Vector3(0, 0, -moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime);
        rocketSprite.localEulerAngles = new Vector2(0, Input.GetAxis("Horizontal") * Time.deltaTime);
    }

    //shoots a bullet in front of  from the player.
    private void ShootBullet(GameObject Bullet)
    {

        Vector2 dir = rocketSprite.transform.position.normalized * (bulletSpeed * Random.Range(1.0f, 1.1f));
        Bullet.GetComponent<Rigidbody2D>().velocity = dir;
        Destroy(Bullet, 2.8f);

    }

    public void TakeDamage(int dmg)
    {
        rocketSprite.GetComponent<SpriteRenderer>().sprite = rocketFlash;
        shipHealth -= dmg; // lower HP
        gameController.UpdateShipHealth(shipHealth);
        Invoke("ResetSprite", 0.05f);
    }

    void ResetSprite()
    {
        rocketSprite.GetComponent<SpriteRenderer>().sprite = rocket;
    }

}
