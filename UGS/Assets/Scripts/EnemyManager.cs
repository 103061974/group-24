using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public GameObject UFOEnemyPrefab;
    public GameObject controller;
    private GameplayController gameController;
    public GameObject Bullet;

    private void Start()
    {
        gameController = controller.GetComponent<GameplayController>();
        SpawnEnemy();
    }

    private void Update()
    {
        if (Random.value < 0.01)
        {
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        int randomNumber = Mathf.RoundToInt(Random.Range(0, 4)); // Random spawnpoint selection //min is inclusive, max is exclusive
        Vector2 spawnPoint;
        switch (randomNumber)
        {
            case 1:
                spawnPoint = RandomTopPos();
                break;
            case 2:
                spawnPoint = RandomBottomPos();
                break;
            case 3:
                spawnPoint = RandomLeftPos();
                break;
            default:
                spawnPoint = RandomRightPos();
                break;
        }

        if (Random.value > 0.25) //75% chance of spawning normal enemy
        {
            GameObject e = Instantiate(EnemyPrefab, spawnPoint, Quaternion.identity); // Spawn enemy
            e.GetComponent<Enemy>().gameController = gameController;
        }
        else
        {
            if (GameObject.FindGameObjectsWithTag("UFO").Length < 2)
            {
                GameObject e = Instantiate(UFOEnemyPrefab, spawnPoint, Quaternion.identity); // Spawn enemy
                e.GetComponent<EnemyUFO>().gameController = gameController;
            }
        }
    }

    private Vector2 RandomTopPos()
    {
        float x, y;
        x = Random.Range(-22, 23);
        y = 13;
        return new Vector2(x, y);
    }

    private Vector2 RandomBottomPos()
    {
        float x, y;
        x = Random.Range(-22, 23);
        y = -18;
        return new Vector2(x, y);
    }

    private Vector2 RandomLeftPos()
    {
        float x, y;
        x = -22;
        y = Random.Range(-14, 15);

        return new Vector2(x, y);
    }

    private Vector2 RandomRightPos()
    {
        float x, y;
        x = 22;
        y = Random.Range(-14, 14);
        return new Vector2(x, y);
    }
}
